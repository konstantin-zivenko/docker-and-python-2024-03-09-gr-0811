import os
from dataclasses import dataclass

from dotenv import load_dotenv

load_dotenv()


@dataclass
class Settings:
    BOT_TOKEN: str = os.environ.get("BOT_TOKEN")
    BOT_NAME: str = os.environ.get("BOT_NAME")
    BOT_USERNAME: str = os.environ.get("BOT_USERNAME")
    DB_NAME: str = os.environ.get("POSTGRES_DB")
    DB_USER: str = os.environ.get("POSTGRES_USER")
    DB_PORT: int = 5432
    DB_PASSWORD: str = os.environ.get("POSTGRES_PASSWORD")
    DB_HOST=os.environ.get("DB_HOST")


settings = Settings()
